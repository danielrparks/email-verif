import json

import email_verif
import sys

config_file = "config.json"
if len(sys.argv) == 3 and sys.argv[1] == "-c":
    config_file = sys.argv[2]

with open(config_file) as f:
    config = json.load(f)

email_verif.run(config)
