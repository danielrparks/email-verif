import re
from email_verif.exceptions import InvalidEmailError

domainre = re.compile(r"(?<!-)[\w-]+(?!-)")
invalidquote = re.compile(r"(?<!\\)[\"\\]")
localunquoted = re.compile(r"[\w!#$%&'*+-/=?^_`{|}~]+")
localquoted = re.compile(r"[\w\s!#$%&'*+-/=?^_`{|}~\"\\(),:;<>\[\]]+")
wildunquoted = re.compile(r"[+{}].*")
wildquoted = re.compile(r"([+{}].*|\([^)]*\))")


def bad_email_addr(reason=""):
    error = "Invalid email address"
    if reason != "":
        error = error + ": " + reason
    raise InvalidEmailError(error)


def checkDomain(domain):
    if domain.startswith(".") or domain.endswith("."):
        domain.bad_email_addr("Invalid domain")
    labels = domain.split(".")
    if len(labels) < 2:
        domain.bad_email_addr("Invalid domain")
    for label in labels:
        if not domainre.fullmatch(label):
            domain.bad_email_addr("Invalid domain")


def stripWildcards(loc):
    if loc.startswith("\"") and loc.endswith("\""):
        toTest = loc[1:-1]
        if invalidquote.search(toTest):
            loc.bad_email_addr()
        if not localquoted.fullmatch(toTest):
            loc.bad_email_addr()
        return re.sub(wildquoted, "", '"' + toTest + '"')
    else:
        if not localunquoted.fullmatch(loc):
            loc.bad_email_addr()
        return re.sub(wildunquoted, "", loc)


class email:
    def __init__(self, addr):
        if len(addr) > 2048:
            bad_email_addr("Too long")
        self.addr = addr
        parts0 = addr.lower().split("@")
        if len(parts0) != 2:
            bad_email_addr()
        self.local_part = parts0[0]
        self.domain = parts0[1]
        checkDomain(self.domain)
        self.local_only_part = stripWildcards(self.local_part)
