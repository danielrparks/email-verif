def defaultConfig(defaultChannel, serverOwner):
    guildRow = dict()
    guildRow["joinChannel"] = 0
    guildRow["verifiedRole"] = 0
    guildRow["adminMessageChannel"] = 0
    guildRow["joinMessage"] = "{mention} Welcome to the server! Please verify your {domain} email address by DMing me the command `email <addr>`."
    guildRow["prefix"] = "ev"
    guildRow["domain"] = "example.com"
    guildRow["ban"] = False
    adminRows = [[serverOwner]]
    channelRows = list()
    if defaultChannel is not None:
        channelRows.append([defaultChannel.id])
        guildRow["adminMessageChannel"] = defaultChannel.id
    return guildConfig(guildRow, adminRows, channelRows)


def singletonsToList(s):
    return [x[0] for x in s]


class guildConfig:
    def __init__(self, guildRow, adminRows, channelRows):
        self.joinChannel = guildRow["joinChannel"]
        self.verifiedRole = guildRow["verifiedRole"]
        self.joinMessage = guildRow["joinMessage"]
        self.adminMessageChannel = guildRow["adminMessageChannel"]
        self.prefix = guildRow["prefix"]
        self.domain = guildRow["domain"]
        self.ban = guildRow["ban"]
        self.adminUsers = singletonsToList(adminRows)
        self.commandChannels = singletonsToList(channelRows)

    def validChannel(self, chId):
        if len(self.commandChannels) == 0:
            return True
        else:
            return chId in self.commandChannels

    def isAdmin(self, member):
        return member.id in self.adminUsers or member.id == member.guild.owner.id

    def config(self, guild):
        result = ""
        jch = guild.get_channel(self.joinChannel)
        if jch is None:
            jch = "UNCONFIGURED"
        else:
            jch = jch.mention
        vr = guild.get_role(self.verifiedRole)
        if vr is None:
            vr = "UNCONFIGURED"
        else:
            vr = vr.name
        amc = guild.get_channel(self.adminMessageChannel)
        if amc is None:
            amc = "UNCONFIGURED"
        else:
            amc = amc.mention
        result += "joinChannel: " + jch + "\n"
        result += "adminMessageChannel: " + amc + "\n"
        result += "verifiedRole: " + vr + "\n"
        result += "joinMessage: \n"
        for line in self.joinMessage.split("\n"):
            result += "> " + line + "\n"
        result += "prefix: " + self.prefix + "\n"
        result += "domain: " + self.domain + "\n"
        result += "ban: " + str(bool(self.ban)) + "\n"
        result += "adminUsers: "
        for user in self.adminUsers:
            m = guild.get_member(user)
            if m is None:
                m = "invalid user"
            else:
                m = m.mention
            result += m + " "
        result += "\n"
        result += "commandChannels: "
        for ch in self.commandChannels:
            c = guild.get_channel(ch)
            if c is None:
                c = "invalid channel"
            else:
                c = c.mention
            result += c + " "
        result += "\n"
        return result
