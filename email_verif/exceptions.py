class PEBKACError(Exception):
    pass


class UnconfiguredError(Exception):
    pass


class InvalidEmailError(Exception):
    pass
