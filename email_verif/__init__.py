import pprint
import sys

import discord
import email_verif.database as database
import email_verif.statictext as statictext
import email_verif.email as email
import smtplib
import traceback
from email_verif.exceptions import *


async def _error(channel, error):
    if channel is None:
        return
    try:
        await channel.send(error)
    except (discord.Forbidden, AttributeError):
        error2 = statictext.badpermserror.format(
            action=" send error message in {server}:\n{message}"
                .format(server=channel.guild.name, message=error))
        try:
            channel.guild.owner.send(error2)
        except discord.Forbidden:
            print(error2, file=sys.stderr)
        except AttributeError:
            pass


async def _safe_action(success, error, channel):
    try:
        await success
    except discord.Forbidden as e:
        await _error(channel, statictext.permserror.format(action=error))
        raise e


async def _check_id(objId, name, msgChannel=None):
    if objId != 0:
        return objId
    else:
        await _error(msgChannel, statictext.badid.format(name=name))
        raise UnconfiguredError(name)


async def _check_nnone(val, name, msgChannel=None):
    if val is None:
        await _error(msgChannel, statictext.noneid.format(name=name))
        raise UnconfiguredError(name)
    else:
        return val


async def _check_get(func, val, name, msgChannel=None):
    return await _check_nnone(func(await _check_id(val, name, msgChannel)), name, msgChannel)


def _format_user(user):
    return user.name + "#" + user.discriminator


class Bot(discord.Client):
    def __init__(self, config, **options):
        super().__init__(**options)
        self.config = config
        self.db = database.database(config["dbinfo"])
        self._mail_login()

    async def on_ready(self):
        for g in self.guilds:
            if g.id not in self.db.guilds:
                await self.on_guild_join(g)

    async def on_error(self, event_method, *args, **kwargs):
        exType = sys.exc_info()[0]
        # ignore exceptions that we deliberately generate to halt the control flow
        if exType != discord.Forbidden and exType != UnconfiguredError:
            print('Ignoring exception in {}'.format(event_method), file=sys.stderr)
            traceback.print_exc()

    async def on_member_join(self, member):
        thisguild = self.db.guilds[member.guild.id]
        errChannel = member.guild.get_channel(thisguild.adminMessageChannel)
        if self.db.isVerified(member.id, thisguild.domain):
            role = await _check_get(member.guild.get_role, thisguild.verifiedRole, "verifiedRole", errChannel)
            await _safe_action(member.add_roles(role), statictext.permserror.format(action="add role"), errChannel)
        else:
            joinChannel = await _check_get(member.guild.get_channel, thisguild.joinChannel, "joinChannel", errChannel)
            await _safe_action(
                joinChannel.send(thisguild.joinMessage.format(mention=member.mention, domain=thisguild.domain)),
                statictext.permserror.format(action="send message"), errChannel)

    async def on_message(self, message):
        if message.author.id != self.user.id:
            if message.channel.type == discord.ChannelType.text:
                await self.processCommand(message)
            elif message.channel.type == discord.ChannelType.private:
                await self.processDirectCommand(message)

    async def on_guild_join(self, guild):
        self.db.defaultConfig(guild.id, guild.system_channel, guild.owner_id)
        if guild.system_channel is not None:
            await _error(guild.system_channel, statictext.unconfiguredmessage)
        else:
            await _error(guild.owner, statictext.unconfiguredmessage)

    async def on_guild_remove(self, guild):
        self.db.removeConfig(guild.id)

    async def processCommand(self, message):
        thisguild = self.db.guilds[message.guild.id]
        splitted = message.content.split()
        errChannel = message.guild.get_channel(thisguild.adminMessageChannel)
        if thisguild.validChannel(message.channel.id) and splitted[0] == thisguild.prefix:
            if splitted[1] == "eval" and message.author.id == self.config["owner"]:
                await message.channel.send(pprint.pformat(eval(" ".join(splitted[2:]))))
            elif splitted[1] == "help" and len(splitted) > 2:
                try:
                    await _safe_action(message.channel.send(statictext.publichelp[splitted[2]]),
                                       statictext.permserror.format(action="send message"), errChannel)
                except KeyError:
                    await _safe_action(message.channel.send(statictext.nosuchcommand),
                                       statictext.permserror.format(action="send message"), errChannel)
            elif splitted[1] == "config":
                if not thisguild.isAdmin(message.author):
                    await _safe_action(message.channel.send(statictext.needadmin),
                                       statictext.permserror.format(action="send message"), errChannel)
                    return
                if len(splitted) == 2:
                    await _safe_action(message.channel.send(self.db.guilds[message.guild.id].config(message.guild)),
                                       statictext.permserror.format(action="send message"), errChannel)
                elif len(splitted) > 3:
                    try:
                        self.db.config(splitted[2], " ".join(splitted[3:]), message)
                        await _safe_action(message.add_reaction(statictext.checkemoji),
                                           statictext.permserror.format(action="react to message"), errChannel)
                    except PEBKACError as error:
                        await _safe_action(message.channel.send(statictext.error.format(error=str(error))),
                                           statictext.permserror.format(action="send message"), errChannel)
                else:
                    await _safe_action(message.channel.send(statictext.missingarg),
                                       statictext.permserror.format(action="send message"), errChannel)
            elif splitted[1] == "notify":
                if not thisguild.isAdmin(message.author):
                    await _safe_action(message.channel.send(statictext.needadmin),
                                       statictext.permserror.format(action="send message"), errChannel)
                    return
                if len(splitted) == 3:
                    thisguild = self.db.guilds[message.guild.id]
                    await _safe_action(message.channel.send(thisguild.joinMessage.format(mention=splitted[2])),
                                       statictext.permserror.format(action="send message"), errChannel)
                    await _safe_action(message.add_reaction(statictext.checkemoji),
                                       statictext.permserror.format("react to message"), errChannel)
                else:
                    await _safe_action(message.channel.send(statictext.missingarg),
                                       statictext.permserror.format(action="send message"), errChannel)
            else:
                if splitted[1] != "help":
                    await _safe_action(message.channel.send(statictext.nosuchcommand),
                                       statictext.permserror.format(action="send message"), errChannel)
                await _safe_action(message.channel.send(statictext.globalhelp),
                                   statictext.permserror.format(action="send message"), errChannel)

    async def add_roles(self, message):
        authorGuilds = set()
        for g in self.db.guilds:
            g2 = self.get_guild(g)
            if g2 is not None:
                u = g2.get_member(message.author.id)
                if u is not None:
                    authorGuilds.add((self.db.guilds[g], g2, u, self.db.guilds[g].verifiedRole))
        for (g, g2, member, role) in authorGuilds:
            try:
                errChannel = g2.get_channel(g.adminMessageChannel)
                role = await _check_get(g2.get_role, role, "verifiedRole", errChannel)
                if role is not None:
                    await _safe_action(member.add_roles(role), statictext.permserror.format(action="add role"),
                                       errChannel)
            except (discord.Forbidden, UnconfiguredError):
                await message.channel.send(statictext.veriffail)
            except discord.DiscordException:
                await message.channel.send(statictext.unknownerror)

    async def processDirectCommand(self, message):
        # who are we gonna tell if we receive an exception here?
        # that's what I thought. don't handle anything
        splitted = message.content.split()
        if splitted[0] == "eval" and message.author.id == self.config["owner"]:
            await message.channel.send(pprint.pformat(eval(" ".join(splitted[1:]))))
        elif splitted[0] == "help" and len(splitted) > 1:
            try:
                await message.channel.send(statictext.privatehelp[splitted[1]])
            except KeyError:
                await message.channel.send(statictext.nosuchcommand)
        elif splitted[0] == "email":
            if len(splitted) < 2:
                await message.channel.send(statictext.missingarg)
                return
            try:
                addr = email.email(splitted[1])
                domains = set()
                for g in self.db.guilds:
                    g2 = self.get_guild(g)
                    if g2 is not None:
                        u = g2.get_member(message.author.id)
                        if u is not None:
                            domains.add(self.db.guilds[g].domain)
                if addr.domain not in domains:
                    await message.channel.send(statictext.wrongdomain)
                    return
                if self.db.isVerified(message.author.id, addr.domain):
                    await message.channel.send(statictext.alreadyverified)
                    await self.add_roles(message)
                else:
                    code = self.db.pending(message.author.id, addr)
                    self._sendmail(addr.addr, statictext.emailmessage.format(fromaddr=self.config["mail"]["username"], toaddr=addr.addr, code=code))
                    await message.channel.send(statictext.sentmail)
            except InvalidEmailError as error:
                await message.channel.send(statictext.error.format(error=str(error)))
        elif splitted[0] == "verify":
            if len(splitted) < 2:
                await message.channel.send(statictext.missingarg)
                return
            result = self.db.verify(message.author.id, splitted[1])
            if result == 1:
                await message.channel.send(statictext.wrongcode)
                return
            elif result >= 2:
                origUser = self.get_user(result)
                if origUser is None:
                    origUser = "unknown user"
                else:
                    origUser = _format_user(origUser)
                for g in self.db.guilds:
                    try:
                        guildObj = self.get_guild(g)
                        if guildObj is not None:
                            badmessage = statictext.baduser.format(copy=_format_user(message.author),
                                                                original=origUser)
                            errChannel = guildObj.get_channel(self.db.guilds[g].adminMessageChannel)
                            if self.db.guilds[g].ban:
                                member = guildObj.get_member(message.author.id)
                                if member is not None:
                                    await _safe_action(guildObj.ban(message.author),
                                                       statictext.permserror.format(action="ban"), errChannel)
                                    badmessage += "\n" + statictext.banmessage
                            await _safe_action((await _check_get(guildObj.get_channel,
                                                                 self.db.guilds[g].adminMessageChannel,
                                                                 "adminMessageChannel", guildObj.owner)).send(badmessage),
                                               statictext.permserror.format(
                                                   action="send message to admin message channel"), guildObj.owner)
                    except discord.DiscordException:
                        pass
                return

            await self.add_roles(message)

        else:
            if splitted[0] != "help":
                await message.channel.send(statictext.nosuchcommand)
            await message.channel.send(statictext.privateglobalhelp)

    def _mail_login(self):
        self.mailer = smtplib.SMTP_SSL(host=self.config["mail"]["host"], port=self.config["mail"]["port"])
        self.mailer.login(self.config["mail"]["username"], self.config["mail"]["password"])

    def _sendmail(self, dest, message):
        for i in range(0, 3):
            try:
                self.mailer.sendmail(self.config["mail"]["username"], [dest], message)
                break
            except (smtplib.SMTPDataError, smtplib.SMTPHeloError, smtplib.SMTPNotSupportedError, smtplib.SMTPSenderRefused):
                self._mail_login()
                continue
            except smtplib.SMTPRecipientsRefused:
                break


def run(config):
    intents = discord.Intents.default()
    intents.members = True
    client = Bot(config, intents=intents)
    client.run(config["token"])
