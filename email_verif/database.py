import sqlite3
import email_verif.guildConfig as guildConfig
from email_verif.exceptions import PEBKACError
import hashlib
import email_verif.statictext as statictext
import time
import os
import distutils.util

schema = """\
CREATE TABLE guilds(guildId INT, joinChannel INT, adminMessageChannel INT, verifiedRole INT, joinMessage TEXT, prefix TEXT, domain TEXT, ban INT);
CREATE TABLE commandChannels(guildId INT, channelId INT);
CREATE TABLE adminUsers(guildId INT, userId INT);
CREATE TABLE pending(userId INT, emailHash BLOB, salt BLOB, domain TEXT, date INT, code BLOB, existingId INT);
CREATE TABLE verified(userId INT, emailHash BLOB, salt BLOB, domain TEXT, date INT);
"""

script_params = {"n": 16384, "r": 8, "p": 1, "dklen": 64}


class database:
    def __init__(self, dbinfo):
        if dbinfo["type"] == "sqlite3":
            self.conn = sqlite3.connect(dbinfo["name"])
            self.conn.row_factory = sqlite3.Row
            cur = self.conn.cursor()
            cur.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='guilds'")
            if cur.fetchone() is None:
                cur.executescript(schema)
        cur = self.conn.cursor()
        cur.execute("DELETE FROM pending WHERE date<?", (int(time.time()) - 60 * 60 * 24 * 30,))
        self.conn.commit()
        self.guilds = self._guildDict()

    def _guildDict(self):
        result = dict()
        cur = self.conn.cursor()
        for row in cur.execute(
                "SELECT guildId, joinChannel, adminMessageChannel, verifiedRole, joinMessage, prefix, domain, ban FROM guilds"):
            cur2 = self.conn.cursor()
            cur2.execute("SELECT userId FROM adminUsers WHERE guildId=?", (row["guildId"],))
            u = cur2.fetchall()
            cur2.execute("SELECT channelId FROM commandChannels WHERE guildId=?", (row["guildId"],))
            c = cur2.fetchall()
            result[row["guildId"]] = guildConfig.guildConfig(row, u, c)
        return result

    def defaultConfig(self, guildId, defaultChannel, serverOwnerId):
        g = guildConfig.defaultConfig(defaultChannel, serverOwnerId)
        self.guilds[guildId] = g
        cur = self.conn.cursor()
        cur.execute("INSERT INTO guilds VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
                    (guildId, g.joinChannel, g.adminMessageChannel, g.verifiedRole, g.joinMessage, g.prefix, g.domain, g.ban))
        if defaultChannel is not None:
            cur.execute("INSERT INTO commandChannels VALUES (?, ?)", (guildId, defaultChannel.id))
        cur.execute("INSERT INTO adminUsers VALUES (?, ?)", (guildId, serverOwnerId))
        self.conn.commit()

    def removeConfig(self, guildId):
        cur = self.conn.cursor()
        cur.execute("DELETE FROM guilds WHERE guildId=?", (guildId,))
        cur.execute("DELETE FROM commandChannels WHERE guildId=?", (guildId,))
        cur.execute("DELETE FROM adminUsers WHERE guildId=?", (guildId,))
        self.conn.commit()

    def config(self, key, value, message):
        cur = self.conn.cursor()
        if key == "joinChannel":
            try:
                c = message.channel_mentions[0].id
                cur.execute("UPDATE guilds SET joinChannel=? WHERE guildId=?", (c, message.guild.id))
                self.guilds[message.guild.id].joinChannel = c
            except IndexError:
                raise PEBKACError(statictext.badchannel)
        elif key == "adminMessageChannel":
            try:
                c = message.channel_mentions[0].id
                cur.execute("UPDATE guilds SET adminMessageChannel=? WHERE guildId=?", (c, message.guild.id))
                self.guilds[message.guild.id].adminMessageChannel = c
            except IndexError:
                raise PEBKACError(statictext.badchannel)
        elif key == "verifiedRole":
            try:
                r = message.role_mentions[0].id
                cur.execute("UPDATE guilds SET verifiedRole=? WHERE guildId=?", (r, message.guild.id))
                self.guilds[message.guild.id].verifiedRole = r
            except IndexError:
                r = value.lower()
                found = False
                for role in message.guild.roles:
                    if role.name.lower() == r:
                        cur.execute("UPDATE guilds SET verifiedRole=? WHERE guildId=?",
                                    (role.id, message.guild.id))
                        self.guilds[message.guild.id].verifiedRole = role.id
                        found = True
                        break
                if not found:
                    raise PEBKACError(statictext.badrole)
        elif key == "joinMessage":
            cur.execute("UPDATE guilds SET joinMessage=? WHERE guildId=?", (value, message.guild.id))
            self.guilds[message.guild.id].joinMessage = value
        elif key == "prefix":
            if value.trim():
                cur.execute("UPDATE guilds SET prefix=? WHERE guildId=?", (value, message.guild.id))
                self.guilds[message.guild.id].prefix = value
            else:
                raise PEBKACError(statictext.badprefix)
        elif key == "domain":
            if "." in value and " " not in value:
                cur.execute("UPDATE guilds SET domain=? WHERE guildId=?", (value, message.guild.id))
                self.guilds[message.guild.id].domain = value
            else:
                raise PEBKACError(statictext.baddomain)
        elif key == "ban":
            try:
                b = bool(distutils.util.strtobool(value))
                cur.execute("UPDATE guilds SET ban=? WHERE guildId=?", (b, message.guild.id))
                self.guilds[message.guild.id].ban = b
            except ValueError:
                raise PEBKACError(statictext.badban)
        elif key == "commandChannels":
            cur.execute("DELETE FROM commandChannels WHERE guildId=?", (message.guild.id,))
            cur.executemany("INSERT INTO commandChannels VALUES (?, ?)",
                            [(message.guild.id, c.id) for c in message.channel_mentions])
            self.guilds[message.guild.id].commandChannels = [c.id for c in message.channel_mentions]
        elif key == "adminUsers":
            cur.execute("DELETE FROM adminUsers WHERE guildId=?", (message.guild.id,))
            cur.executemany("INSERT INTO adminUsers VALUES (?, ?)",
                            [(message.guild.id, u.id) for u in message.mentions])
            self.guilds[message.guild.id].commandChannels = [u.id for u in message.mentions]
        else:
            raise PEBKACError("no such configuration option")
        self.conn.commit()

    def pending(self, uId, email):
        salt = os.urandom(32)
        dk = hashlib.scrypt(password=email.local_only_part.encode("utf-8"), salt=salt, **script_params)
        code = os.urandom(3).hex()
        cur = self.conn.cursor()
        cur.execute("DELETE FROM pending WHERE userId=?", (uId,))  # prevent concurrent verification
        cur.execute("INSERT INTO pending VALUES (?, ?, ?, ?, ?, ?, ?)",
                    (uId, dk, salt, email.domain, int(time.time()), code, self._validEmail(uId, email)))
        self.conn.commit()
        return code

    def _validEmail(self, uId, email):
        cur = self.conn.cursor()
        for row in cur.execute("SELECT userId, emailHash, salt FROM verified WHERE domain=? AND userId!=?",
                               (email.domain, uId)):
            dk = hashlib.scrypt(password=email.local_only_part.encode("utf-8"), salt=row["salt"], **script_params)
            if dk == row["emailHash"]:
                return row["userId"]
        return 0

    def verify(self, uId, code):
        cur = self.conn.cursor()
        cur.execute("SELECT emailHash, salt, domain, date, existingId FROM pending WHERE userId=? AND code=?", (uId, code))
        row = cur.fetchone()
        if row is None:
            return 1
        else:
            if row["existingId"]:
                return row["existingId"]
            else:
                cur.execute("DELETE FROM pending where userId=?", (uId,))
                cur.execute("INSERT INTO verified VALUES (?, ?, ?, ?, ?)",
                            (uId, row["emailHash"], row["salt"], row["domain"], row["date"]))
                self.conn.commit()
                return 0

    def isVerified(self, uId, domain):
        cur = self.conn.cursor()
        cur.execute("SELECT userId FROM verified WHERE userId=? AND domain=?", (uId, domain))
        return cur.fetchone() is not None
