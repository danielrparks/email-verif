nosuchcommand = "No such command"
needadmin = "You need to be an administrator of the server to perform this action"
error = "Error: {error}"
missingarg = "Missing argument for command, see help"
wrongcode = "Invalid authentication code"
wrongdomain = "You're not in any servers that expect you to verify for this domain"
publichelp = {
        "help": "see `help help`",
        "config": "(no arguments): view configuration for this server\n<key value>: set configuration for this server",
        "notify": "<mention>: notify specified users to complete verification as if they had joined the server"
        }
privatehelp = {
        "help": "see `help help`",
        "email": "provide an email for verification",
        "verify": "verify an email using the code sent to it"
        }
helpappend = "\ntry `help <command>` to get help for a specific command"
globalhelp = "Valid commands: help [command], config [<key> <value>], notify <mention>" + helpappend
privateglobalhelp = "Valid commands: help [command], email <addr>, verify <code>" + helpappend
emailmessage = """\
From: email-verif bot <{fromaddr}>
To: {toaddr}
Subject: Verify your email

You have been requested to verify your email to join a Discord server using the email-verif bot.

Send the following command to email-verif in your direct message channel to complete verification:

verify {code}
"""
sentmail = "An email has been sent to that address with instructions on how to verify"
checkemoji = "✅"
badchannel = "incorrect channel specified"
badrole = "incorrect role specified"
badprefix = "need to specify prefix"
baddomain = "invalid domain specified"
badban = "that was a yes or no question. Maybe is not an option."
badconfig = "no such configuration option"
permserror = "bad permissions: unable to {action}"
badpermserror = "**severely broken permissions**: unable to {action}"
badid = "object unconfigured: {name}"
noneid = "object missing, try reconfiguring: {name}"
unconfiguredmessage = """\
This bot must be configured to work correctly in your server.
Do `ev help config` and `ev config` for more information.
"""
baduser = "MULTIACCOUNT DETECTED: user {copy} tried to verify with the same email as {original}"
banmessage = "this user has been banned"
veriffail = "Failed to verify you due to a server permissions error. This error has been automatically reported to the administrators of the server."
unknownerror = "An unknown error has occurred. Please try again later or complain to the devs."
alreadyverified = "You have already verified this email! Re-adding roles."
